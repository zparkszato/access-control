const ACTIONS = require('./test/actions')
const ACTION_TYPES = require('./test/actionTypes')
const GROUPS = require('./test/groups')
const USERS = require('./test/users')

const AccessControl = require('./AccessControl')

describe('Usage tests', () => {
  const ac = new AccessControl(ACTION_TYPES, ACTIONS, GROUPS)

  test('Initializes correctly', () => {
    const $group = ac.getGroup('0')
    expect($group).toBeInstanceOf(ac.Group)
    expect($group.id).toBe(GROUPS[0].id)
  })

  test('Handles permissions correctly', async () => {
    const user = USERS[1]
    const canUserEditOwnPost = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: user.id, group: '1' }, group: GROUPS[1] })
    const canUserEditOthersPost = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: '789', group: '1' }, group: GROUPS[1] })
    const canUserEditOwnPostInOtherGroup = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: '789', group: '0' }, group: GROUPS[1] })
    expect(canUserEditOwnPost).toBe(true)
    expect(canUserEditOthersPost).toBe(false)
    expect(canUserEditOwnPostInOtherGroup).toBe(false)
  })

  test('Handles admin permissions correctly', async () => {
    const user = USERS[0]
    const canUserEditOwnPost = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: user.id, group: '0' }, group: GROUPS[0] })
    const canUserEditOthersPostInMyGroup = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: '789', group: '0' }, group: GROUPS[0] })
    const canUserEditOwnPostInOtherGroup = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: user.id, group: '1' }, group: GROUPS[0] })
    const canUserEditOtherPostInOtherGroup = await ac.canUserDo(user.id, ac.actionTypes.editPosts, { post: { owner: '789', group: '1' }, group: GROUPS[0] })
    expect(canUserEditOwnPost).toBe(true)
    expect(canUserEditOthersPostInMyGroup).toBe(false)
    expect(canUserEditOwnPostInOtherGroup).toBe(true)
    expect(canUserEditOtherPostInOtherGroup).toBe(true)
  })
})