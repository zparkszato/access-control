class User {
  constructor(user, groups=[]) {
    this.userInput = user
    this.id = user.id
    this.username = user.username
    this.email = user.email
    this._groups = {}
    this.groups = groups
    return this
  }

  toObject() {
    return this.userInput
  }

  get groups() {
    return Object.values(this._groups)
  }

  set groups(groups) {
    for (let group of groups) {
      this.addGroup(group)
    }
    return this
  }

  getGroup(id) {
    return this._groups[id]
  }

  addGroup(group) {
    this._groups = { ...this._groups, [group.id.toString()]: group }
    return this
  }

  removeGroup(group) {
    delete this._groups[group.id.toString()]
    return this
  }

}

module.exports = User