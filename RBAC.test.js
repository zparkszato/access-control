const RBAC = require('./RBAC')

const roles = {
  admin: {
    can: [ 'edit' ],
    inherits: [ 'manager' ]
  },
  manager: {
    can: [ 'publish' ],
    inherits: [ 'writer' ],
  },
  writer: {
    can: [ 
      'write',
      {
        name: 'edit',
        when: (params, cb) => {
          setImmediate(cb, null, params.user.id === params.post.owner)
        },
      }
    ],
    inherits: [ 'guest' ]
  },
  guest: {
    can: [ 'read' ]
  }
}

describe('Usage tests', () => {
  const rbac = new RBAC(roles)

  const testRole = async role => {
    try {
      const obj = {
        read: await rbac.canBool(role, 'read'),
        write: await rbac.canBool(role, 'write'),
        editOwn: await rbac.canBool(role, 'edit', { user: { id: 'guy' }, post: { owner: 'guy' } }),
        editOther: await rbac.canBool(role, 'edit', { user: { id: 'guy' }, post: { owner: 'other guy' } }),
        publish: await rbac.canBool(role, 'publish'),
      }
      return obj
    } catch(err) {
      console.error(err)
    }
  }

  test('Guest can read, but cannot write, edit, or publish', async () => {
    const results = await testRole('guest')
    expect(results).toEqual({
      read: true,
      editOwn: false,
      editOther: false,
      publish: false,
      write: false,
    })
  })

  test('Writer can read, write and edit his own posts, but cannot publish', async () => {
    const results = await testRole('writer')
    expect(results).toEqual({
      read: true,
      editOwn: true,
      editOther: false,
      publish: false,
      write: true,
    })
  })

  test('Manager can read, write and edit his own posts, and publish', async () => {
    const results = await testRole('manager')
    expect(results).toEqual({
      read: true,
      editOwn: true,
      editOther: false,
      publish: true,
      write: true,
    })
  })

  test('Admin can read, write and edit any post, and publish', async () => {
    const results = await testRole('admin')
    expect(results).toEqual({
      read: true,
      editOwn: true,
      editOther: true,
      publish: true,
      write: true,
    })
  })
})