const ACTIONS = require('./test/actions')
const ACTION_TYPES = require('./test/actionTypes')
const GROUPS = require('./test/groups')
const USERS = require('./test/users')

const withActions = require('./withActions')
const GroupClass = require('./Group')
const UserClass = require('./User')

class Group extends withActions(GroupClass, ACTIONS, ACTION_TYPES) {}
class User extends withActions(UserClass, ACTIONS, ACTION_TYPES) {}

test('Returns an object of instance Group', () => {
  const group = new Group(GROUPS[1])
  expect(group).toBeInstanceOf(Group)
})

test('Handles roles correctly', async () => {
  const group = new Group(GROUPS[1])
  const canManagerPublishThisGroup = (
    await group.can(
      'manager', 
      ACTION_TYPES.publishPosts, 
      { 
        group: GROUPS[1], 
        post: { group: '1' } 
      }
    )
  )
  expect(canManagerPublishThisGroup).toBe(true)
})

test('Handles roles correctly for groups with additional permissions', async () => {
  const group = new Group(GROUPS[0])
  const canGuestPublishAnyGroup = await group.can('guest', ACTION_TYPES.publishPosts, { 
    group: GROUPS[0], 
    post: { group: '1' } 
  })
  const canGuestPublishOwnGroup = await group.can('guest', ACTION_TYPES.publishPosts, { 
    group: GROUPS[0], 
    post: { group: '0' } 
  })
  expect(canGuestPublishAnyGroup).toBe(true)
  expect(canGuestPublishOwnGroup).toBe(false)
})

describe('User tests', () => {
  const user = new User(USERS[1])
  let group

  beforeEach(() => {
    group = new Group(GROUPS[1])
  })

  test('Initializes correctly from group input', () => {
    expect(group.members).toEqual([ { id: user.id, role: 'manager' } ])
    expect(group.getRoleForUser(user.id)).toBe('manager')
    expect(group.getUsersWithRole('manager')).toEqual([ user.id ])
  })

  test('Can add user to group', () => {
    group.addMember(user.id, 'manager')
    expect(group.members).toEqual([ { id: user.id, role: 'manager' } ])
    expect(group.getRoleForUser(user.id)).toBe('manager')
    expect(group.getUsersWithRole('manager')).toEqual([ user.id ])
  })

  test('Can remove user from group', () => {
    group.removeMember(user.id)
    expect(group.members).toEqual([])
    expect(group.getRoleForUser(user.id)).toBeFalsy()
    expect(group.getUsersWithRole('manager')).toEqual([])
  })

  test('Can query user permissions for valid group user', async () => {
    const canUserPublishAnyGroup = await group.canUserDo(user.id, ACTION_TYPES.publishPosts, { post: { group: '0' } })
    const canUserPublishOwnGroup = await group.canUserDo(user.id, ACTION_TYPES.publishPosts, { post: { group: '1' } })
    const canUserEditAnyPost = await group.canUserDo(user.id, ACTION_TYPES.editPosts, { post: { owner: '123', group: '1' } })
    const canUserEditOwnPost = await group.canUserDo(user.id, ACTION_TYPES.editPosts, { post: { owner: '456', group: '1' } })
    expect(canUserPublishAnyGroup).toBe(false)
    expect(canUserPublishOwnGroup).toBe(true)
    expect(canUserEditAnyPost).toBe(false)
    expect(canUserEditOwnPost).toBe(true)
  })

  test('Querying permissions for user not in group returns false', async () => {
    const user2 = new User(USERS[0])
    const canReadPosts = await group.canUserDo(user2.id, ACTION_TYPES.readPosts, { post: { owner: '789', group: '1' } })
    expect(canReadPosts).toBe(false)
  })
})

