const Q = require('q')

const UserClass = require('./User')
const GroupClass = require('./Group')
const withActions = require('./withActions')

class AccessControl {
  constructor(actionTypes, actions, groups, users) {
    this.actionTypes = actionTypes
    this.actions = actions

    class Group extends withActions(GroupClass, this.actions, this.actionTypes) {}
    class User extends withActions(UserClass, this.actions, this.actionTypes) {}

    this.Group = Group
    this.User = User

    this._groups = {}
    this._users = {}

    this.init(groups, users)

    return this
  }

  get groups() {
    return Object.values(this._groups)
  }

  set groups(newGroups) {
    this._groups = {}
    this.init(newGroups)
  }

  getGroup(id) {
    return this._groups[id]
  }

  init(groups, users) {
    for (let group of groups) {
      this.addGroup(group)
    }
    return this
  }

  canUserDo(userId, operation, params, cb) {
    return (
      Q.all(this.groups.map(group => group.canUserDo(userId, operation, params, cb)))
      .then((res=[]) => res.some(e => e) ? true : false)
      .catch(err => console.error(err))
    )
  }

  // async userCanDoForGroups(userId, operation, params, cb) {
  //   let allowedGroups = []
  //   for (let group of this.groups) {
  //     if (await group.canUserDo(userId, operation, params, cb)) allowedGroups.push(group.id)
  //   }
  //   return allowedGroups
  // }
  
  addGroup(groupInput) {
    this._groups[groupInput.id] = new this.Group(groupInput)
  }

}

module.exports = AccessControl