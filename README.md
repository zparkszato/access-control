# Group Access Control

This module provides arbitrary access control based on a set of provided actions and a group schema.  It exposes the `AccessControl` class, which handles all user access control functionality under the hood via its `canUserDo` method.



## AccessControl

This class should be set up once at the top level and be passed down to all modules that make use of it.  It takes three parameters:

- **actionTypes:** A list of available action types.  This should be a simple key/value object with strings as values.
- **actions:** A specification for those action types, which handles parameters, validation, etc.  The schema for this is below.
- **groups:** A schema that outlines the groups that exist, permissions for those groups, and any roles those groups have.  The schema for this is outlined below.



#### Actions

Actions specify how to handle and validate each Action Type.  The action specification should be an object, where the keys match the available Action Types, and the values are an object with the following options:



- **params:** A list of params that this action takes.  Each param should be an object that takes the following options.
  - **type:** The type the param should be.  Should be a Javascript primitive type of the following types: *Array*, *String*, *Number*, or *Boolean*.
  - **required:** Boolean that indicates whether the param is required or not.  If not required, checking for that param is skipped when checking for access control.  Defualts to false.
  - **children:** If the type is *Array*, this is a params object that specifies type checking for the array's children.
  - **validator:** A function that determines whether or not the user can do the action.  Takes two parameters: the value of the parameter itself in the group role schema, and the parameters passed with the request.  Its return value must be a boolean.



An example action definition would be:



```javascript
{
  [actionTypes.readPosts]: {
    params: {
      groups: {
        type: Array,
        required: false,
        children: {
          type: String
        },
        validator: (self=[], params) => {
          if (self.includes('not-self') && params.post.group === params.group.id) return false
          if (self.includes('*')) return true
          if (self.includes('self') && params.post.group === params.group.id) return true
          if (self.includes(params.post.group)) return true
          return false
        }
      }
    }
  },
  [actionTypes.writePosts]: {
    params: {
      groups: {
        type: Array,
        required: false,
        children: {
          type: String
        },
        validator: (self=[], params) => {
          if (self.includes('not-self') && params.post.group === params.group.id) return false
          if (self.includes('*')) return true
          if (self.includes('self') && params.post.group === params.group.id) return true
          if (self.includes(params.post.group)) return true
          return false
        }
      }
    }
  },
}
```





#### Groups

Group schemas lay out what users within that group can do, what roles are available for each group, what each role can do, who the group members are, and what their role is.  Using the Action specification above, a possible group specification could be:



```javascript
[
  {
    id: "123",
    name: "Example Group",
    members: [
      {
        id: "user1",
        role: "user"
      }
    ]
    roles: {
    	// Base user can only read posts for this group
      user: {
    		can: [
    			{
            name: actionTypes.readPosts,
            params: {
              groups: [ "self" ],
            }
          }
    		]
  		},
      // Writer can write posts for this group, as well as read them
      writer: {
    		can: [
    			{
            name: actionTypes.writePosts,
            params: {
              groups: [ "self" ],
            }
          }
    		],
        inherits: [ "user" ]
  		},
    }
  }
]
```





