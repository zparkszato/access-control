module.exports = (superClass, actions, actionTypes) => {
  return class extends superClass{
    get actions() {
      return actions
    }
    get actionTypes() {
      return actionTypes
    }
  }
}