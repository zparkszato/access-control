const UserClass = require('./User')
const GroupClass = require('./Group')
const withActions = require('./withActions')

const GROUPS = require('./test/groups')
const USERS = require('./test/users')
const ACTIONS = require('./test/actions')
const ACTION_TYPES = require('./test/actionTypes')

class Group extends withActions(GroupClass, ACTIONS, ACTION_TYPES) {}
class User extends withActions(UserClass, ACTIONS, ACTION_TYPES) {}

const userProfile = USERS[0]

const groups = [
  new Group(GROUPS[1])
]

describe('Usage tests', () => {
  test('Initializes correctly', () => {
    const user = new User(userProfile)
    expect(user.username).toBe(userProfile.username)
    expect(user.id).toBe(userProfile.id)
    expect(user.email).toBe(userProfile.email)
  })

  test('Sets up groups correctly', () => {
    const user = new User(userProfile, groups)
    expect(user.groups).toEqual(groups)
    expect(user.getGroup('1')).toEqual(groups[0])
    // expect(groups[0].members).toEqual([ user ])
  })

  test('Can add groups to user', () => {
    const user = new User(userProfile, [])
    user.addGroup(groups[0])
    expect(user.groups).toEqual(groups)
    expect(user.getGroup('1')).toEqual(groups[0])
    // expect(groups[0].members).toEqual([ user ])
  })

  test('Can remove group from user', () => {
    const user = new User(userProfile, groups)
    user.removeGroup(groups[0])
    expect(user.groups).toEqual([])
    // expect(groups[0].members).toEqual([])
  })



  // DO HERE
})