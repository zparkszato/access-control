const actionTypes = require('./actionTypes')

const usersParam = {
  type: Array,
  required: true,
  children: {
    type: String
  },
  validator: (self=[], params) => {
    if (self.includes('*')) return true
    if (self.includes('self') && params.post.owner === params.user.id) return true
    if (!self.includes(params.post.owner)) return false
    return true
  }
}

const groupsParam = {
  type: Array,
  required: false,
  children: {
    type: String
  },
  validator: (self=[], params) => {
    if (self.includes('not-self') && params.targetGroupId === params.group.id) return false
    if (self.includes('*')) return true
    if (self.includes('self') && params.targetGroupId === params.group.id) return true
    if (self.includes(params.targetGroupId)) return true
    return false
  }
}

const includeParam = {
  type: Array,
  required: false,
  children: {
    type: String
  },
  validator: (self=[], params) => {
    if (self.length < 1) return false
    if (self.includes('*')) return true
    return self.includes(params.role.name) ? true : false
  }
}

const excludeParam = {
  type: Array,
  required: false,
  children: {
    type: String
  },
  validator: (self=[], params) => {
    if (self.length < 1) return true
    if (self.includes('*')) return false
    return (!self.includes(params.role.name) ? true : false)
  }
}

const groupParamForPosts = {
  ...groupsParam,
  validator: (self, params) => groupsParam.validator(self, { ...params, targetGroupId: params.post.group })
}

const groupParamForGroupAttrs = {
  ...groupsParam,
  validator: (self, params) => groupsParam.validator(self, { ...params, targetGroupId: params.targetGroup.id })
}

module.exports = {
  [actionTypes.readPosts]: {
    params: {
      groups: groupParamForPosts,
    }
  },
  [actionTypes.writePosts]: {
    params: {
      groups: groupParamForPosts,
    }
  },
  [actionTypes.editPosts]: {
    params: {
      users: usersParam,
      groups: groupParamForPosts,
    }
  },
  [actionTypes.addGroupMembers]: {
    params: {
      groups: groupParamForGroupAttrs,
    }
  },
  [actionTypes.publishPosts]: {
    params: {
      users: usersParam,
      groups: groupParamForPosts,
    }
  },
  [actionTypes.editGroupRoles]: {
    params: {
      include: includeParam,
      exclude: excludeParam,
      groups: groupParamForGroupAttrs,
    }
  },
}