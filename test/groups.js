const actionTypes = require('./actionTypes')

const users = require('./users')

const defaultRoles = {
  superuser: {
    can: [
      {
        name: actionTypes.editGroupRoles,
        params: {
          include: [ '*' ],
          groups: [ 'self' ],
        }
      }
    ],
    inherits: [ 'admin' ]
  },
  admin: {
    can: [
      {
        name: actionTypes.addGroupMembers,
        params: {
          groups: [ 'self' ]
        }
      },
      {
        name: actionTypes.editGroupRoles,
        params: {
          exclude: [ 'superuser' ],
          groups: [ 'self' ],
        }
      },
      {
        name: actionTypes.editPosts,
        params: {
          users: [ '*' ],
          groups: [ 'self' ],
        }
      }
    ],
    inherits: [ 'manager' ]
  },
  manager: {
    can: [
      {
        name: actionTypes.publishPosts,
        params: {
          users: [ '*' ],
          groups: [ 'self' ],
        }
      }
    ],
    inherits: [ 'editor' ]
  },
  editor: {
    can: [
      actionTypes.writePosts,
      {
        name: actionTypes.editPosts,
        params: {
          users: [ 'self' ],
          groups: [ 'self' ],
        }
      }
    ],
    inherits: [ 'guest' ]
  },
  guest: {
    can: [
      {
        name: actionTypes.readPosts,
        params: {
          groups: [ 'self' ],
        }
      }
    ],
  }
}

const groups = [
  {
    id: '0',
    name: 'Admin',
    roles: { ...defaultRoles },
    members: [
      {
        id: users[0].id,
        role: 'manager'
      } 
    ],
    permissions: [
      {
        name: actionTypes.editGroupRoles,
        params: {
          include: [ '*' ],
          groups: [ '*', 'not-self' ]
        },
      },
      {
        name: actionTypes.addGroupMembers,
        params: {
          include: [ '*' ],
          groups: [ '*', 'not-self' ]
        }
      },
      {
        name: actionTypes.editPosts,
        params: {
          users: [ '*' ],
          groups: [ '*', 'not-self' ]
        }
      },
      {
        name: actionTypes.publishPosts,
        params: {
          users: [ '*' ],
          groups: [ '*', 'not-self' ]
        }
      },
      {
        name: actionTypes.writePosts,
        params: {
          users: [ '*' ],
          groups: [ '*', 'not-self' ]
        }
      },
      {
        name: actionTypes.readPosts,
        params: {
          groups: [ '*', 'not-self' ]
        }
      }
    ]
  },
  {
    id: '1',
    name: 'Test Group',
    roles: { ...defaultRoles },
    members: [
      {
        id: users[1].id,
        role: 'manager'
      } 
    ],
  }
]

module.exports = groups