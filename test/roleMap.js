const actionTypes = require('./actionTypes')

module.exports = {
  superuser: {
    can: [
      {
        name: actionTypes.editGroupRoles,
        params: {
          include: [ '*' ]
        }
      }
    ],
    inherits: [ 'admin' ]
  },
  admin: {
    can: [
      actionTypes.addGroupMembers,
      {
        name: actionTypes.editGroupRoles,
        params: {
          exclude: [ 'superuser' ]
        }
      },
      {
        name: actionTypes.editPosts,
        params: {
          users: [ '*' ]
        }
      }
    ],
    inherits: [ 'manager' ]
  },
  manager: {
    can: [
      {
        name: actionTypes.publishPosts,
        params: {
          users: [ '*' ]
        }
      }
    ],
    inherits: [ 'editor' ]
  },
  editor: {
    can: [
      actionTypes.writePosts,
      {
        name: actionTypes.editPosts,
        params: {
          users: [ 'self' ]
        }
      }
    ],
    inherits: [ 'guest' ]
  },
  guest: {
    can: [ actionTypes.readPosts ],
  }
}