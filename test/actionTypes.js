module.exports = {
  readPosts: 'readPosts',
  editPosts: 'editPosts',
  writePosts: 'writePosts',
  publishPosts: 'publishPosts',
  addGroupMembers: 'addGroupMembers',
  editGroupRoles: 'editGroupRoles',
}