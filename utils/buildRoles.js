const validateParams = require('./validateParams')

const buildRoles = (actions, roleMap) => {
  let roles = {}

  for (let [ name, def ] of Object.entries(roleMap)) {
    let role = {
      can: [],
    }

    // If role inherits, pass that through
    role.inherits = def.inherits || []

    if (roleMap._base && name !== '_base') role.inherits = [ ...role.inherits, '_base' ]

    if (def.can.length > 0) {
      for (let perm of def.can) {
        if (typeof perm === 'string') {
          role.can.push(perm)
        } else if (typeof perm === 'object') {
          let out = { name: perm.name }

          if (perm.params && Object.entries(perm.params).length > 0) {
            try {

              validateParams(perm.params, actions[perm.name].params)

              out.when = (params, cb) => {
                try {
                  const can = Object.entries({...perm.params}).every(([ name, p ]) => actions[perm.name].params[name].validator(p, params))
                  return can ? cb(null, true) : cb(null, false)
                } catch(err) {
                  return cb(err)
                }
              }
            } catch(err) {
              throw err
            }
          }
          role.can.push(out)
        }
      }
    }
    roles[name] = role
  }
  
  return roles
}

module.exports = buildRoles

