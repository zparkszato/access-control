const validateParam = (name, param, spec) => {
  if (spec.type === String && !(typeof param === 'string')) throw new TypeError(`Param ${name} must be type String`)
  if (spec.type === Number && !(typeof param === 'number')) throw new TypeError(`Param ${name} must be type Number`)
  if (spec.type === Boolean && !(typeof param === 'boolean')) throw new TypeError(`Param ${name} must be type Boolean`)
  if (spec.type === Array && !Array.isArray(param)) throw new TypeError(`Param ${name} must be type Array`)

  if (spec.type === Array && Array.isArray(param)) {
    if (!spec.children) return true
    try {
      if (param.every((p, i) => validateParam(`${name}.children[${i}]`, p, spec.children))) return true
    } catch(err) {
      throw err
    }
  }

  if (spec.enum && !spec.enum.includes(param)) throw new TypeError(`Param ${name} must have value [ ${spec.enum.join(', ')} ]`)

  return true
}

const validateParams = (params, specs) => {
  for (let [ name, spec ] of Object.entries(specs)) {
    if (spec.required && !params[name]) throw new Error(`Param ${name} is required, but not provided.`)
    if (params[name]) {
      try {
        validateParam(name, params[name], spec)
      } catch(err) {
        throw err
      }
    }
  }
}

module.exports = validateParams