const RBAC = require('./RBAC')
const User = require('./User')

const { buildRoles } = require('./utils')

const buildRoleMemberMap = (members, memberRoleMap) => {
  let out = {}
  for (let user of Object.values(members)) {
    out[memberRoleMap[user.id]] = [
      ...(out[memberRoleMap[user.id]] || []),
      user
    ]
  }
  return out
}

class Group {
  constructor(group) {
    this.groupInput = group
    this.name = group.name
    this.id = group.id
    this._members = {}
    this._memberRoleMap = {}
    this._roleMemberMap = {}
    
    this._roles = buildRoles(this.actions || {}, { ...group.roles, _base: { can: [ ...(group.permissions || []) ] } })
    this.rbac = new RBAC(this._roles)
    this.can = (...args) => this.rbac.canBool(...args)

    this.members = group.members || []
  }

  toObject() {
    return this.groupInput
  }

  get members() {
    return Object.values(this._members)
  }

  get roles() {
    return Object.keys(this._roles).filter(e => e !== '_base')
  }

  set members(newMembers=[]) {
    if (!Array.isArray(newMembers)) throw new TypeError('Members must be an array')
    for (let user of newMembers) {
      if (!user.id) throw new Error('User input must have a id')
      if (!user.role) throw new Error('User input must have a role')
      if (!this._roles[user.role]) throw new Error(`Role "${user.role}" is not in the roles for this group`)

      this.addMember(user.id, user.role)
    }
    return this
  }

  async canUserDo(userId, operation, baseParams, cb) {
    const role = this._memberRoleMap[userId]
    if (!role) return false
    const params = { ...baseParams, group: this.toObject(), user: { id: userId } }
    return await this.can(role, operation, params, cb)
  }

  getUsersWithRole(role) {
    if (!this._roles[role]) throw new Error(`Role "${role}" is not in the roles for this group`)
    return this._roleMemberMap[role] ? this._roleMemberMap[role].map(e => e.id) : []
  }

  getRoleForUser(userId) {
    return this._memberRoleMap[userId]
  }

  addMember(userId, role) {
    if (!userId) throw new Error('User id is required')
    if (!role) throw new Error('Role is required')
    if (!this._roles[role]) throw new Error(`Role "${role}" is not in the roles for this group`)

    this._members[userId] = { id: userId, role }
    this._memberRoleMap[userId] = role
    this._roleMemberMap = buildRoleMemberMap(this._members, this._memberRoleMap)

    // user.addGroup(this)

    return this
  }

  removeMember(userId) {
    if (!userId) throw new Error('User id is required')
    delete this._members[userId]
    delete this._memberRoleMap[userId]
    this._roleMemberMap = buildRoleMemberMap(this._members, this._memberRoleMap)

    // user.removeGroup(this)

    return this
  }
}

module.exports = Group