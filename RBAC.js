const Q = require('q')

class RBAC {
  constructor(roles) {
    this.init(roles)
  }

  init(roles) {
    if (typeof roles === 'function') {
      this._init = (
        Q.nfcall(roles)
        .then(data => this.init(data))
      )
      return
    }

    if (typeof roles !== 'object') throw new TypeError('Expected an object as input')

    this.roles = roles

    let map = {}
    Object.keys(roles).forEach(role => {
      map[role] = {
        can: {}
      }
      if (roles[role].inherits) {
        map[role].inherits = roles[role].inherits
      }

      roles[role].can.forEach(operation => {
        if (typeof operation === 'string') {
          map[role].can[operation] = 1
        } else if (typeof operation.name === 'string' && typeof operation.when === 'function') {
          map[role].can[operation.name] = operation.when
        }
      })
    })

    this.roles = map
    this._inited = true
  }

  canBool(role, operation, params, cb) {
    // console.log({ role, operation, params })
    return Q.Promise((resolve, reject) => {
      this.can(role, operation, params, cb)
      .then(val => resolve(val ? true : false))
      .catch(err => {
        // console.error(err)
        resolve(false)
      })
    })
  }

  can(role, operation, params, cb) {
    // console.log({ role, operation, params })
    if (!this._inited) {
      return (
        this._init
        .then(() => this.can(role, operation, params, cb))
      )
    }

    if (typeof params === 'function') {
      cb = params
      params = undefined
    }

    let callback = cb || (() => {})

    return Q.Promise((resolvePromise, rejectPromise) => {
      const resolve = result => {
        // console.log({ role, operation, params, result })
        resolvePromise(result)
        callback(undefined, result)
      }
      const reject = err => {
        rejectPromise(err)
        callback(err)
      }

      if (typeof role !== 'string') throw new TypeError('Expected first parameter to be string : role')
      if (typeof operation !== 'string') throw new TypeError('Expected second parameter to be string : operation')

      let $role = this.roles[role]

      if (!$role) throw new Error('Undefined role')
      if (!$role.can[operation]) {
        if (!$role.inherits) return reject(false)
        
        return (
          Q.all($role.inherits.map(childRole => this.canBool(childRole, operation, params)))
          .then(res => res.some(e => e) ? resolve(true) : reject(false))
          .catch(err => reject(err))
          // Q.any($role.inherits.map(childRole => this.can(childRole, operation, params)))
          // .then(resolve, reject)
        )
      }

      if ($role.can[operation] === 1) return resolve(true)

      if (typeof $role.can[operation] === 'function') {
        $role.can[operation](params, (err, result) => {
          if (err) return reject(err)
          if (!result) return reject(false)
          resolve(true)
        })
        return
      }

      reject(false)
    })
  }
}

module.exports = RBAC